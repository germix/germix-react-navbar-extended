# Germix React Navbar - Extended

## About

Germix react extended navbar components

## Installation

```bash
npm install @germix/germix-react-navbar-extended
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
