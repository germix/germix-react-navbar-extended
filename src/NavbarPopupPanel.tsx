import React from 'react';

import
    { Popup
    , PopupWrapper
} from '@germix/germix-react-core';
import NavbarBadge from './NavbarBadge';

interface Props
{
    open?,
    icon?,
    title?,
    badgeCount?,
    onOpen(),
    onClose(),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarPopupPanel extends React.Component<Props, State>
{
    render()
    {
        const { open } = this.props;

        return (
<PopupWrapper>
    <div className={"navbar-button" + (open ? ' active' : '') }
        onClick={(e) =>
        {
            this.props.onOpen();
        }}
    >
        <i className={this.props.icon}></i>
        <NavbarBadge count={this.props.badgeCount}/>
    </div>
    {!open
        ?
        null
        :
        <Popup
            arrow={true}
            anchor='bottom-center'
            horz_policy='left'
            vert_policy='down'
            closeable={false}
            onClose={() =>
            {
                this.props.onClose();
            }}
        >
            <div className="navbar-popup-panel">
                { this.props.children }
            </div>
        </Popup>
    }
</PopupWrapper>
        );
    }
}
export default NavbarPopupPanel;
