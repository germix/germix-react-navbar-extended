import React from 'react';

interface Props
{
    count?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarBadge extends React.Component<Props, State>
{
    render()
    {
        return (
<>
{ this.props.count > 0 &&
    <div className="navbar-badge">
        { this.props.count }
    </div>
}
</>
        );
    }
}
export default NavbarBadge;
