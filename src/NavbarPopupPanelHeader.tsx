import React from 'react';

interface Props
{
    title,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarPopupPanelHeader extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-popup-panel-head">
    { this.props.title }
</div>
        );
    }
}
export default NavbarPopupPanelHeader;
