import React from 'react';

interface Props
{
    highlight?: boolean;
    onClick?: React.MouseEventHandler<HTMLDivElement>;
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarPopupPanelItem extends React.Component<Props, State>
{
    render()
    {
        return (
<div className={"navbar-popup-panel-item" + (this.props.highlight ? ' highlight' : '')} onClick={this.props.onClick}>
    { this.props.children}
</div>
        );
    }
}
export default NavbarPopupPanelItem;
