import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarPopupPanelFooter extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-popup-panel-footer">
    { this.props.children }
</div>
        );
    }
}
export default NavbarPopupPanelFooter;
