import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarPopupPanelBody extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-popup-panel-body">
    { this.props.children }
</div>
        );
    }
}
export default NavbarPopupPanelBody;
